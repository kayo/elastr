#ifndef __ELASTR_H__
#define __ELASTR_H__ "elastr.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifndef ESTR_BITS
#define ESTR_BITS 32
#endif

#if ESTR_BITS == 8
typedef int8_t estr_sint_t;
typedef uint8_t estr_uint_t;
#elif ESTR_BITS == 16
typedef int16_t estr_sint_t;
typedef uint16_t estr_uint_t;
#elif ESTR_BITS == 32
typedef int32_t estr_sint_t;
typedef uint32_t estr_uint_t;
typedef float estr_real_t;
#elif ESTR_BITS == 64
typedef int64_t estr_sint_t;
typedef uint64_t estr_uint_t;
typedef double estr_real_t;
#endif

/**
 * @brief The string type
 */
typedef struct estr estr_t;

/**
 * @brief The string resize function
 */
typedef int estr_resize_t(estr_t *str, size_t len);

/**
 * @brief The string struct
 */
struct estr {
  estr_resize_t *res;
  size_t len;
  char *ptr;
}; /* 12 bytes */

/**
 * @brief Get pointer to C-string
 */
#define estr_str(str) ((str)->ptr)

/**
 * @brief Get length of C-string
 */
#define estr_len(str) ((str)->len)

/**
 * @brief Get end of C-string
 */
#define estr_end(str) (estr_str(str) + estr_len(str))

/**
 * @brief Get the pointer in string
 */
#define estr_ptr(str, idx) (((idx) < 0 ? estr_end(str) : estr_str(str)) + (idx))

/**
 * @brief Initialize string using external buffer
 */
void estr_extern_init(estr_t *str, char *ptr);

/**
 * @brief Initialize string using heap memory
 */
void estr_heap_init(estr_t *str, char *ptr);

/**
 * @brief The static buffer for string
 */
typedef struct {
  size_t len;
  char ptr[0];
} estr_static_t;

/**
 * @brief Define static buffer for string
 */
#define estr_static(name, size)   \
  struct {                        \
    size_t len;                   \
    char ptr[size];               \
  } name = {                      \
    size,                         \
    ""                            \
  }

/**
 * @brief Initialize string using static buffer
 */
void estr_static_init(estr_t *str, estr_static_t *buf);

/**
 * @brief Release string data
 */
static inline void estr_release(estr_t *str) {
  str->res(str, 0);
}

/**
 * @brief Resize string
 */
static inline int estr_resize(estr_t *str, size_t len) {
  return str->res(str, len);
}

/**
 * @brief Extend string
 */
static inline int estr_extend(estr_t *str, size_t len) {
  return estr_resize(str, estr_len(str) + len);
}

/**
 * @brief Reduce string
 */
static inline int estr_reduce(estr_t *str, size_t len) {
  return estr_resize(str, estr_len(str) - len);
}

/**
 * @brief Cut string to length
 */
static inline void estr_cut(estr_t *str, size_t len) {
  str->len = len;
  if (str->ptr != NULL)
    str->ptr[len] = '\0';
}

/**
 * @brief Put C-string to end of string
 */
int estr_putsn(estr_t *str, const char *ptr, int len);

/**
 * @brief Put string to end of string
 */
#define estr_putes(str, estr) estr_putsn(str, estr_str(estr), estr_len(estr))

/**
 * @brief Put constant C-string to end of string
 */
#define estr_putsc(str, cstr) estr_putsn(str, cstr, sizeof(cstr)-1)

/**
 * @brief Put null-terminated C-string to end of string without processing
 */
static inline int estr_putsr(estr_t *str, const char *nstr) {
  return estr_putsn(str, nstr, strlen(nstr));
}

/**
 * @brief Put C-string to end of string with escaping
 */
int estr_putse(estr_t *str, const char *ptr, size_t len);

/**
 * @brief Put null-terminated C-string to end of string with escaping
 */
static inline int estr_putsq(estr_t *str, const char *nstr) {
  return estr_putse(str, nstr, strlen(nstr));
}

/**
 * @brief Put string data as hexademical string
 */
int estr_putsx(estr_t *str, const char *ptr, size_t len);

/**
 * @brief Put single character to end of string
 */
int estr_putc(estr_t *str, char chr);

/**
 * @brief Put unsigned int as decimal number to end of string
 */
int estr_putud(estr_t *str, estr_uint_t val);

/**
 * @brief Put unsigned int as hexademical number to end of string
 */
int estr_putux(estr_t *str, estr_uint_t val);

/**
 * @brief Put unsigned int as octal number to end of string
 */
int estr_putuo(estr_t *str, estr_uint_t val);

/**
 * @brief Put signed int as decimal number to end of string
 */
int estr_putsd(estr_t *str, estr_sint_t val);

#if ESTR_BITS >= 32
/**
 * @brief Put real number to end of string
 */
int estr_putrp(estr_t *str, estr_real_t val, uint8_t exp);
#endif

#endif/*__ELASTR_H__*/
