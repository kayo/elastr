#include "test.h"
#include "../elastr.h"

#define test_xtos(tag, val, str)                                  \
  {                                                               \
    info("estr_put" #tag " " str);                                \
    estr_t estr;                                                  \
    estr_extern_init(&estr, buf);                                 \
    int r = estr_put##tag(&estr, val);                            \
    /*if (r != sizeof(str)-1) {                                   \
      fail("Invalid return value! Expected: %d Actual: %d",       \
           sizeof(str)-1, r);                                     \
           } else */if (strcmp(str, estr_str(&estr)) != 0) {      \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, estr_str(&estr));                                 \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
    estr_release(&estr);                                          \
  }

#define test_ftos(exp, val, str)                                  \
  {                                                               \
    info("estr_putrp " str);                                      \
    estr_t estr;                                                  \
    estr_extern_init(&estr, buf);                                 \
    int r = estr_putrp(&estr, val, exp);                          \
    if (r != sizeof(str)-1) {                                     \
      fail("Invalid return value! Expected: %d Actual: %d",       \
           sizeof(str)-1, r);                                     \
    } else if (strcmp(str, estr_str(&estr)) != 0) {               \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, estr_str(&estr));                                 \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
    estr_release(&estr);                                          \
  }

int main() {
  char buf[32];
  
  test_xtos(ud, 0, "0");
  test_xtos(ud, 1, "1");
  test_xtos(ud, 13, "13");
  test_xtos(ud, 10, "10");
  test_xtos(ud, 100, "100");
  test_xtos(ud, 110, "110");
  test_xtos(ud, 200, "200");
  test_xtos(ud, 123, "123");
  test_xtos(ud, 250, "250");
  test_xtos(ud, 251, "251");
  test_xtos(ud, 100u, "100");
#if ESTR_BITS >= 16
  test_xtos(ud, 2015, "2015");
  test_xtos(ud, 1000u, "1000");
  test_xtos(ud, 10000u, "10000");
#endif
#if ESTR_BITS >= 32
  test_xtos(ud, 12032105, "12032105");
  test_xtos(ud, 100000u, "100000");
  test_xtos(ud, 1000000u, "1000000");
  test_xtos(ud, 10000000u, "10000000");
  test_xtos(ud, 100000000u, "100000000");
  test_xtos(ud, 1000000000u, "1000000000");
#endif
#if ESTR_BITS >= 64
  test_xtos(ud, 10000000000u, "10000000000");
  test_xtos(ud, 100000000000u, "100000000000");
  test_xtos(ud, 1000000000000u, "1000000000000");
  test_xtos(ud, 10000000000000u, "10000000000000");
  test_xtos(ud, 100000000000000u, "100000000000000");
  test_xtos(ud, 1000000000000000u, "1000000000000000");
  test_xtos(ud, 10000000000000000u, "10000000000000000");
  test_xtos(ud, 100000000000000000u, "100000000000000000");
  test_xtos(ud, 1000000000000000000u, "1000000000000000000");
  test_xtos(ud, 10000000000000000000u, "10000000000000000000");
#endif
  
  test_xtos(sd, 0, "0");
  test_xtos(sd, 1, "1");
  test_xtos(sd, -1, "-1");
  test_xtos(sd, 13, "13");
  test_xtos(sd, 10, "10");
  test_xtos(sd, 100, "100");
  test_xtos(sd, -10, "-10");
  test_xtos(sd, -100, "-100");
  test_xtos(sd, 123, "123");
  test_xtos(sd, -123, "-123");
#if ESTR_BITS >= 16
  test_xtos(sd, 2015, "2015");
  test_xtos(sd, -2015, "-2015");
#endif
#if ESTR_BITS >= 32
  test_xtos(sd, 12032105, "12032105");
  test_xtos(sd, -2301051, "-2301051");
#endif

  test_xtos(ux, 0, "0");
  test_xtos(ux, 1, "1");
  test_xtos(ux, 10, "a");
  test_xtos(ux, 15, "f");
  test_xtos(ux, 16, "10");
  test_xtos(ux, 18, "12");
  test_xtos(ux, 29, "1d");
#if ESTR_BITS >= 16
  test_xtos(ux, 0x10e, "10e");
  test_xtos(ux, 0xf230, "f230");
#endif
#if ESTR_BITS >= 32
  test_xtos(ux, 0x10000, "10000");
  test_xtos(ux, 0x100000, "100000");
  test_xtos(ux, 0x1000000, "1000000");
  test_xtos(ux, 0x10000000, "10000000");
#endif
#if ESTR_BITS >= 64
  test_xtos(ux, 0x100000000, "100000000");
  test_xtos(ux, 0x1000000000, "1000000000");
  test_xtos(ux, 0x10000000000, "10000000000");
  test_xtos(ux, 0x100000000000, "100000000000");
  test_xtos(ux, 0x1000000000000, "1000000000000");
  test_xtos(ux, 0x10000000000000, "10000000000000");
  test_xtos(ux, 0x100000000000000, "100000000000000");
  test_xtos(ux, 0x1000000000000000, "1000000000000000");
#endif

  test_xtos(uo, 0, "0");
  test_xtos(uo, 1, "1");
  test_xtos(uo, 10, "12");
  test_xtos(uo, 15, "17");
  test_xtos(uo, 16, "20");
  test_xtos(uo, 18, "22");
  test_xtos(uo, 29, "35");
#if ESTR_BITS >= 16
  test_xtos(uo, 0416, "416");
  test_xtos(uo, 0171060, "171060");
#endif
#if ESTR_BITS >= 32
  test_xtos(uo, 01000000, "1000000");
  test_xtos(uo, 010000000, "10000000");
  test_xtos(uo, 0100000000, "100000000");
  test_xtos(uo, 01000000000, "1000000000");
  test_xtos(uo, 010000000000, "10000000000");
#endif
#if ESTR_BITS >= 64
  test_xtos(uo, 0100000000000, "100000000000");
  test_xtos(uo, 01000000000000, "1000000000000");
  test_xtos(uo, 010000000000000, "10000000000000");
  test_xtos(uo, 0100000000000000, "100000000000000");
  test_xtos(uo, 01000000000000000, "1000000000000000");
  test_xtos(uo, 010000000000000000, "10000000000000000");
  test_xtos(uo, 0100000000000000000, "100000000000000000");
  test_xtos(uo, 01000000000000000000, "1000000000000000000");
  test_xtos(uo, 010000000000000000000, "10000000000000000000");
  test_xtos(uo, 0100000000000000000000, "100000000000000000000");
  test_xtos(uo, 01000000000000000000000, "1000000000000000000000");
#endif

#if ESTR_BITS >= 32
  test_ftos(10, 0, "0");
  test_ftos(10, 1, "1");
  test_ftos(10, -1, "-1");
  test_ftos(10, 1234, "1234");
  test_ftos(10, -1234, "-1234");
  test_ftos(7, 1.001, "1.001");
  test_ftos(7, -1.001, "-1.001");
  test_ftos(10, 0.00001, "0.00001");
  test_ftos(4, 1000.0001, "1000.0001");
  test_ftos(4, -1000.0001, "-1000.0001");
#endif
  
  return 0;
}
