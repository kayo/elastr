#include <stdio.h>

#define D "\x1b[0m"
#define Q "\x1b[1;37m"
#define R "\x1b[1;31m"
#define G "\x1b[1;32m"
#define B "\x1b[1;34m"

#define info(fmt, ...) printf(Q "????" D B " Test " fmt D, ##__VA_ARGS__)
#define pass() printf("\r" G "PASS" D "\n")
#define fail(fmt, ...) printf("\r" R "FAIL" D "\n" fmt "\n", ##__VA_ARGS__)
