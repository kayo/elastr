#include "elastr.h"

/* elastic string in heap memory */

#include <malloc.h>
#include <stdlib.h>

#define i2h(h) ((h) < 10 ? (h) + '0' : (h) + ('a' - 10))

#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

static int estr_extern_resize(estr_t *str, size_t len) {
  (void)str;
  (void)len;
  
  return 0;
}

void estr_extern_init(estr_t *str, char *ptr) {
  str->res = estr_extern_resize;
  str->len = 0;
  str->ptr = ptr;
  *str->ptr = '\0';
}

#define roundup32(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))

static int estr_heap_resize(estr_t *str, size_t len) {
  if (len > (str->ptr ? malloc_usable_size(str->ptr) : 0)) {
    roundup32(len);
    char *tmp = (char*)realloc(str->ptr, len);
    
    if (tmp != NULL) {
      str->ptr = tmp;
      if (str->len == 0)
        *str->ptr = '\0';
    } else {
      return -1;
    }
  } else if (len == 0) {
    str->ptr = realloc(str->ptr, len);
    str->len = 0;
  }
  
  return 0;
}

void estr_heap_init(estr_t *str, char *ptr) {
  str->res = estr_heap_resize;
  str->ptr = ptr;
  str->len = ptr == NULL ? 0 : strlen(ptr);
}

#define containerof(ptr, type, member) ((type*)((char*)(ptr) - offsetof(type, member)))

/* elastic string in static memory */

static int estr_static_resize(estr_t *str, size_t len) {
  estr_static_t *buf = containerof(str->ptr, estr_static_t, ptr);
  
  if (len > buf->len) {
    return -1;
  } else if (len == 0) {
    str->len = 0;
  }
  
  return 0;
}

void estr_static_init(estr_t *str, estr_static_t *buf) {
  str->res = estr_static_resize;
  str->len = 0;
  str->ptr = buf->ptr;
  *str->ptr = '\0';
}

/* elastic string builder functions */

int estr_putsn(estr_t *str, const char *ptr, int len) {
  size_t new_len = str->len + len;
  
  if (0 != str->res(str, new_len + 1))
    return -1;
  
  memcpy(str->ptr + str->len, ptr, len);
  
  str->len = new_len;
  str->ptr[new_len] = '\0';
  
  return len;
}

int estr_putsx(estr_t *str, const char *ptr, size_t len) {
	len <<= 1;
	size_t new_len = str->len + len;
	
	if (0 != str->res(str, new_len + 1))
    return -1;

	char *dst = str->ptr + str->len;
	char *end = str->ptr + new_len;
	
  for (; dst < end; ) {
    *dst++ = i2h((*ptr >> 4) & 0x0f);
    *dst++ = i2h(*ptr++ & 0x0f);
  }

	str->len = new_len;
  str->ptr[new_len] = '\0';
	
	return len;
}

int estr_putse(estr_t *str, const char *ptr, size_t len) {
	/* prepare */
  const char *c = ptr;
	const char *e = ptr + len;
	
	len = 0;
	
	for (; c < e; c++) {
		if (unlikely(*c == '\0' ||
					(*c >= '\a' && *c <= '\r') ||
					*c == '\\' ||
					*c == '"' ||
					*c == '\'')) {
			len += 2;
		} else {
			len ++;
		}
  }

	size_t new_len = str->len + len;
	
	if (0 != str->res(str, new_len + 1))
    return -1;
	
  /* put string data */
	c = ptr;
	char *d = str->ptr + str->len;

	static const char r[] = "abtnvfr";
	
  for (; c < e; c++) {
    if (unlikely(*c == '\0')) {
      *d++ = '\\';
			*d++ = '0';
      continue;
    } else if (unlikely(*c >= '\a' && *c <= '\r')) {
      *d++ = '\\';
			*d++ = r[*c - '\a'];
      continue;
    } else if (unlikely(*c == '"' ||
                        *c == '\'' ||
                        *c == '\\')) {
      *d++ = '\\';
    }
    *d++ = *c;
  }
  
	str->len = new_len;
  str->ptr[new_len] = '\0';
	
  return len;
}

int estr_putc(estr_t *str, char chr) {
  return estr_putsn(str, &chr, 1);
}

/* fast estimation count of decimal digits in numeric value */
static uint8_t estr_digud(estr_uint_t val) {
#define r(res) return res;
#define c(exp, lt, ge) if (val < (1e##exp)) { lt } else { ge }
#if ESTR_BITS >= 8
#define c8   \
  c(1,       \
    r(1),    \
    c(2,     \
      r(2),  \
      r(3)))
#endif
#if ESTR_BITS >= 16
#define c16     \
  c(2,          \
    c(1,        \
      r(1),     \
      r(2)),    \
    c(3,        \
      r(3),     \
      c(4,      \
        r(4),   \
        r(5))))
#endif
#if ESTR_BITS >= 32
#define c32         \
  c(5,              \
    c16,            \
    c(7,            \
      c(6,          \
        r(6),       \
        r(7)),      \
      c(8,          \
        r(8),       \
        c(9,        \
          r(9),     \
          r(10)))))
#endif
#if ESTR_BITS >= 64
#define c64             \
  c(10,                 \
    c32,                \
    c(15,               \
      c(12,             \
        c(11,           \
          r(11),        \
          r(12)),       \
        c(13,           \
          r(13),        \
          c(14,         \
            r(14),      \
            r(15)))),   \
      c(17,             \
        c(16,           \
          r(16),        \
          r(17)),       \
        c(18,           \
          r(18),        \
          c(19,         \
            r(19),      \
            r(20))))))
#endif
  
#if ESTR_BITS == 8
  c8;
#elif ESTR_BITS == 16
  c16;
#elif ESTR_BITS == 32
  c32;
#elif ESTR_BITS == 64
  c64;
#endif

#ifdef c8
#undef c8
#endif
#ifdef c16
#undef c16
#endif
#ifdef c32
#undef c32
#endif
#ifdef c64
#undef c64
#endif
#undef r
#undef c
}

static uint8_t estr_digux(estr_uint_t val) {
  uint8_t dig = 0;
  
  for (; val > 0; dig++, val >>= 4);
  
  return dig > 0 ? dig : 1;
}

static uint8_t estr_diguo(estr_uint_t val) {
  uint8_t dig = 0;
  
  for (; val > 0; dig++, val >>= 3);
  
  return dig > 0 ? dig : 1;
}

#define putdig(str, val, dig, mod) {            \
    char *b = str->ptr + str->len;              \
    char *p = b + dig;                          \
                                                \
    *p-- = '\0';                                \
                                                \
    goto r1;                                    \
  r0:                                           \
    val /= mod;                                 \
  r1:                                           \
    *p-- = i2h(val % mod);                      \
                                                \
    if (p >= b) {                               \
      if (val > 0)                              \
        goto r0;                                \
    } else                                      \
      goto r3;                                  \
  r2:                                           \
    *p-- = '0';                                 \
                                                \
    if (p >= b)                                 \
      goto r2;                                  \
  r3:                                           \
    str->len += dig;                            \
  }

static void _estr_putud(estr_t *str, estr_uint_t val, uint8_t dig) {
  putdig(str, val, dig, 10);
}

int estr_putud(estr_t *str, estr_uint_t val) {
  uint8_t dig = estr_digud(val);
  
  if (0 != str->res(str, str->len + dig + 1))
    return -1;
  
  _estr_putud(str, val, dig);
  
  return dig;
}

int estr_putux(estr_t *str, estr_uint_t val) {
  uint8_t dig = estr_digux(val);
  
  if (0 != str->res(str, str->len + dig + 1))
    return -1;
  
  putdig(str, val, dig, 16);
  
  return dig;
}

int estr_putuo(estr_t *str, estr_uint_t val) {
  uint8_t dig = estr_diguo(val);
  
  if (0 != str->res(str, str->len + dig + 1))
    return -1;
  
  putdig(str, val, dig, 8);
  
  return dig;
}

int estr_putsd(estr_t *str, estr_sint_t val) {
  uint8_t neg = val < 0 ? 1 : 0;
  
  if (val < 0)
    val = -val;
  
  uint8_t dig = estr_digud(val);
  
  if (0 != str->res(str, str->len + dig + neg + 1))
    return -1;
  
  if (neg)
    estr_putc(str, '-');
  
  _estr_putud(str, val, dig);
  
  return dig + neg;
}

#if ESTR_BITS >= 32
static estr_sint_t f2i(estr_real_t val, estr_uint_t exp){
  for (; exp > 0; exp--) val *= 10;
  
  estr_real_t afp = val - (estr_sint_t)val;
  
  return val + (afp < -0.5 ? -1 : afp < 0.5 ? 0 : 1);
}

int estr_putrp(estr_t *str, estr_real_t val, uint8_t exp){
  estr_sint_t ipart = val;
  estr_sint_t fpart = f2i(val - ipart, exp);
  
  for (; fpart != 0 && (fpart % 10) == 0; fpart /= 10, exp --);
  
  if (fpart < 0) {
    fpart = -fpart;
  }
  
  int res = estr_putsd(str, ipart);
  
  if (res < 0 || fpart < 1 || exp < 1) {
    return res;
  }
  
  if (0 != str->res(str, str->len + exp + 2))
    return -1;
  
  str->ptr[str->len++] = '.';
  
  _estr_putud(str, fpart, exp);
  
  return res + exp + 1;
}
#endif
